package com.protridentechnologies.callus.activity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.protridentechnologies.callus.R;
import com.protridentechnologies.callus.database.DatabaseHelper;
import com.protridentechnologies.callus.fragment.AddressFragment;
import com.protridentechnologies.callus.fragment.PickFragment;
import com.protridentechnologies.callus.model.User;
import com.protridentechnologies.callus.utils.CustPrograssbar;
import com.protridentechnologies.callus.utils.SessionManager;

import butterknife.BindView;

import static com.protridentechnologies.callus.utils.SessionManager.LOGIN;
import static com.protridentechnologies.callus.utils.Utiles.isSelect;

public class PickupDropActivity extends BaseActivity {
    @BindView(R.id.picklayout1)
    LinearLayout picklayout1;
    Button pickaddresbtn,dropaddresbtn,taskButton;
    User user;
    public static CustPrograssbar custPrograssbar;
    public static PickupDropActivity pickupDropActivity = null;
    Fragment fragment1 = null;
    DatabaseHelper databaseHelper;
    SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_pick);
        sessionManager = new SessionManager(PickupDropActivity.this);
        custPrograssbar = new CustPrograssbar();
        databaseHelper = new DatabaseHelper(PickupDropActivity.this);
        sessionManager = new SessionManager(PickupDropActivity.this);
        user = sessionManager.getUserDetails("");
        pickupDropActivity = this;
        pickaddresbtn = findViewById(R.id.pickaddrbtn);
        dropaddresbtn = findViewById(R.id.dropaddrbtn);
        taskButton=findViewById(R.id.addtaskbtn);


        taskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PickupDropActivity.this, Pick_Task_Activity.class));
            }
        });

        pickaddresbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment;
                if (sessionManager.getBooleanData(LOGIN)) {
                    isSelect = false;
                    fragment = new AddressFragment();
                    callFragment(fragment);
                } else {
                    startActivity(new Intent(PickupDropActivity.this, LoginActivity.class));
                }
            }
        });
        dropaddresbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment;
                if (sessionManager.getBooleanData(LOGIN)) {
                    isSelect = false;
                    fragment = new AddressFragment();
                    callFragment(fragment);
                } else {
                    startActivity(new Intent(PickupDropActivity.this, LoginActivity.class));
                }
            }
        });

    }


    public static PickupDropActivity getInstance() {
        return pickupDropActivity;
    }

   public void callFragment(Fragment fragmentClass) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.pickFrame, fragmentClass).addToBackStack("adds").commit();
    }


}