package com.protridentechnologies.callus.activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import com.protridentechnologies.callus.R;
public class Pick_Task_Activity extends BaseActivity{
    Spinner spinner_task;
    TextView confirm;
    String[] task={"Food","Clothes","Documents"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick__task_);
        spinner_task=findViewById(R.id.spinner_task);
        ArrayAdapter adp=new ArrayAdapter(this,android.R.layout.simple_spinner_item,task);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_task.setAdapter(adp);

        confirm=findViewById(R.id.btn_task_confirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Pick_Task_Activity.this, PickupDropActivity.class));
            }
        });


    }


}